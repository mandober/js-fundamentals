'use strict';

// MODULES

// modules_pattern_01.js

// pattern 1: Revealing Module pattern

function User(){
    
    var username, 
        password;
    
    function doLogin(user,pw) {
        username = user;
        password = pw;
        
        // the rest of the login work
        // ...
    } 
    
    return publicAPI = {
        login: doLogin
    };
    
}


// create a `User` module instance
var fred = User();

fred.login( "fred", "12Battery34!" );


// Executing User() creates an instance of the User module -- a whole new scope is
// created, and thus a whole new copy of each of these inner variables/functions. We assign
// this instance to fred. If we run User() again, we'd get a new instance entirely separate
// from fred.





// module singleton pattern

var foo = (function CoolModule() {
    
    var something = "cool",
        another = [1, 2, 3];
    
    function doSomething() {
        console.log(something);
    }
    
    function doAnother() {
        console.log(another.join( " ! " ));
    }
    
    return {
        doSomething: doSomething,
        doAnother: doAnother
    };
    
})();

foo.doSomething(); // cool
foo.doAnother(); // 1 ! 2 ! 3




// modern modules

//module dependency loader/manager

var MyModules = (function Manager() {
    
    var modules = {};
    
    function define(name, deps, impl) {
        for (var i=0; i<deps.length; i++) {
            deps[i] = modules[deps[i]];
        } 
        
        modules[name] = impl.apply( impl, deps );
    }
    
    function get(name) {
        return modules[name];
    } 
    
    return {
        define: define,
        get: get
    };
    
})();
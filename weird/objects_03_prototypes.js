"use strict";

/*
 * OBJECTS
 * objects_03_prototypes.js
 */


// [[Prototype]]



var anotherObject = {
    a: 2
};

// create an object linked to `anotherObject`
var myObject = Object.create(anotherObject);

console.log('myObject.a : ', myObject.a);




// Creating Links among objects

var foo = {
    something: function() {
        console.log( "Tell me something good..." );
    }
};

var bar = Object.create(foo);
bar.something(); // Tell me something good...


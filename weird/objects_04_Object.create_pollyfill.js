"use strict";

/*
 * Object.create
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create
 */


// Object.create(proto[, propertiesObject])


// pollyfill (cannot implement second arg logic in pre ES6)

if (typeof Object.create != 'function') {                       // if not already implemented. Or: if(!Object.create){
    
    Object.create = (function(undefined) {                      // assign IIFE to Object.create
        
        var Temp = function() {};                               // throw-away function
        
        return function (prototype, propertiesObject) {
            if (prototype !== null && prototype !== Object(prototype)) {
                throw TypeError(
                    'Argument must be an object, or null'
                );
            }
            
            Temp.prototype = prototype || {};                   // overriding Temp's `.prototype` property to point to the object we want to link to
            
            var result = new Temp();                            // will create [[Prototype]] link from `Temp` to `result`
            
            Temp.prototype = null;
            
            if (propertiesObject !== undefined) {
                Object.defineProperties(result, propertiesObject); 
            } 

            if (prototype === null) {                           // to imitate the case of `Object.create(null)`
                result.__proto__ = null;
            }
            
            return result;                                      // return linked object
            
        };
        
    })();
    
}



// an utility is better suited since Object.create can't be completley pollyfilled (2. arg cannot be implemented in pre ES6)

function createAndLinkObject(o) {
    function F(){}
    F.prototype = o;
    return new F();
}
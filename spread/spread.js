// spread
function sumAll(a, b, c) {
    return a + b + c;
}

var numbers = [6, 7, 8];

// ES5 way of passing array as an argument of a function (abusing apply)
console.log(sumAll.apply(null, numbers)); // 21

// ES6 Spread operator
console.log(sumAll(...numbers)); // 21

// spread with arrays
var midweek = ['Wed', 'Thu'];
var weekend = ['Sat', 'Sun'];
var week = ['Mon', 'Tue', ...midweek, 'Fri', ...weekend];
console.log(week); // ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]

/*
 * OBJECTS
 * objects.js
 */

// create empty object with constructor
var constructedObj = new Object();
// add properties
constructedObj.a = 12;

// object literal (prefered way)
var literalObj = {
    a: 12,
    b: 'abc'
};


// Computed Property Names
var computedPropertyObject = { };

computedPropertyObject[true] = "foo";
computedPropertyObject[3] = "bar";
computedPropertyObject[computedPropertyObject] = "baz";

console.log('computedPropertyObject["true"]', computedPropertyObject["true"]);            // "foo"
console.log('computedPropertyObject["3"]', computedPropertyObject["3"]);               // "bar"
console.log('computedPropertyObject["[object Object]"]', computedPropertyObject["[object Object]"]); // "baz"


// ES6 adds computed property names, where you can specify an expression, 
// surrounded by a [ ] pair, in the key-name position of an object-literal declaration:
var prefix = "foo";
var computedNameObject = {
    [prefix + "bar"]: "hello",
    [prefix + "baz"]: "world"
};
console.log(computedNameObject["foobar"]);
console.log(computedNameObject.foobar);











/*
 * inspect the object sub-type
 */
function tell(obj) {
    console.log(Object.prototype.toString.call(obj));
}



// string object
var strObject = new String( "I am a string" );
typeof strObject;                              // "object"
strObject instanceof String;                   // true
tell(strObject);                               // [object String]


// function
function anotherFunction(a, b) {
    tell(anotherFunction);  // [object Function]
    tell(arguments);        // [object Arguments]
}
anotherFunction(1, 2);


// array
var anotherArray = [1, 2, 3];
tell(anotherArray);  // [object Array]
//Arrays are objects, so even though each index is a positive integer, 
//you can also add properties onto the array:
anotherArray.baz = 'foo';
console.log('anotherArray.length: ' + anotherArray.length); // still 3
console.log('anotherArray.baz: ' + anotherArray.baz);       // "baz"


// plain object
var anotherObject = {
    a: 'starter',
    b: 'ender',
    c: true,
    d: 42
};


// object with refrences
var myObject = {
    a: 2,
    b: anotherObject, // reference, not a copy!
    c: anotherArray, // another reference!
    d: anotherFunction
}

anotherArray.push( anotherObject, myObject );


// Copy object via JSON
// works only for copying objects with plain properties (shallow copy)
var newObj2 = JSON.parse( JSON.stringify( anotherObject ) );


// Object.assign(..)
// takes a target object as its first parameter, and one or more source objects as its 
// subsequent parameters. It iterates over all the ENUMERABLE, OWNED KEYS (immediately 
// present) on the source object(s) and copies them (via = assignment only) to target.
// It also RETURNS target.

// Object.assign is ES6 specific for copying objects with refrences included (deep copy)
var newObj = Object.assign( {}, myObject );

console.log('newObj.a: ', newObj.a);  // 2
console.log('newObj.b === anotherObject :', newObj.b === anotherObject);  // true
//newObj.c === anotherArray; // true
//newObj.d === anotherFunction; // true


brandNewObj = {
    z: 12,
    x: 144
};
Object.assign(brandNewObj, myObject);


/* ARRAY

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

* exotic object: integer indices (max: 2**32-2) and auto-updated `length` property
* arrays, like all objects, is always passed by reference
* array is initialized with the given elements, except in the case where a single argument is passed to the
  Array constructor and that argument is a number.
* arrayLength: If the only argument passed to the Array() constructor is an integer between 0 and 2**32-1(incl),
  mthis returns a new array with its length property set to that number (Note: this implies an array of
  arrayLength empty slots, not slots with actual undefined values). If the argument is any other number,
  a RangeError exception is thrown.
* Arrays cannot use strings as element indexes (as in an associative array), but must use integers.
  Setting or accessing via non-integers using bracket notation (or dot notation) will not set or retrieve an
  element from the array list itself, but will set or access a variable associated with that array's object
  property collection.
*/

//::: Arrays are always passed by reference:
var arr1 = [1, 2, 3];
var arr2 = arr1; // variables, arr1 and arr2, share a reference to the [1, 2, 3] value
arr2.push(4);
arr1; // [1,2,3,4]
arr2; // [1,2,3,4]
arr1 === arr2; // true

//::: Array constructor
var arr1 = new Array(1, 2, 3, 4); // [1,2,3,4]
var arr2 = new Array(4); // [<4 empty elements>]




var ray = [11, 'hi', 3.14];
ray = ['abc', 'def'];
ray = [1, 2, 3];
ray[6] = 12;
ray; // [1, 2, 3, undefined × 3, 12]

// length
var l = ray.length;

// slice
var s = ray.slice(1, 3); // ["hi", 3.14]

// push
ray.push('ghi');

// concat
var rays = ray.concat(ray2);

// join
rays2 = ray.join(', ');

// reverse
var rev = ray.reverse();

// sort
var sorted = ray.sort();

// map
var ch = ray.map(function (el) {
    return el + ' element';
});



// forEach()
// executes a provided function once for each array element
['a', 'b', 'c'].forEach(function(element) {
    console.log(element); // a // b // c
});
// or:
['a', 'b', 'c'].forEach(element => console.log(element));


// some()
// whether some element in the array passes the test implemented by the provided function.

function isBiggerThan10(element, index, array) {
    return element > 10;
}
[2, 5, 8, 1, 4].some(isBiggerThan10); // false
[12, 5, 8, 1, 4].some(isBiggerThan10); // true
// or:
[2, 5, 8, 1, 4].some((element, index, array) => element > 10); // false


// every()
// tests whether all elements in the array pass the test implemented by the provided function.
function isBigEnough(element, index, array) {
    return element >= 10;
}
[12, 5, 8, 130, 44].every(isBigEnough);   // false
[12, 54, 18, 130, 44].every(isBigEnough); // true
// or:
[2, 5, 8, 1, 4].every((element, index, array) => element < 10); // true


// iterator
var myArray = [1, 2, 3];
var it = myArray[Symbol.iterator]();
it.next();	//	{	value:1,	done:false	}
it.next();	//	{	value:2,	done:false	}
it.next();	//	{	value:3,	done:false	}
it.next();	//	{	done:true	}

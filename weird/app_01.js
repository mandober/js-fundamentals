var person = new Object();

person["first"] = "Nina";
person["last"] = "Mayers";

var firstname = "first";

console.log(person["first"]);
console.log(person[firstname]);
console.log(person.first);

// literal object
agent = {
    first: "Jack",
    last: "Bauer",
    assoc: {
        first: "Chloe",
        last: "O'Bryan"
    }

};

console.log(agent);

function send(person) {
    console.log("Dispatched to: " + person.first);
}

send(agent);

send({
    first: "Brian",
    last: "Shapel"
});



/**
 * functional programing
 */

// adding properties to function object
function greet() {
    console.log("hi");
}

greet.lang = "eng";
console.log(greet);
console.log(greet.lang);


// passing a function to function
function log(a) {
    a();
}

log(function() {
    console.log('hi');
})


// THIS

var a = {
    name: "Name of 'a' object",
    log: function(){
        this.name = "Overidden name of 'a' object"
        // `this` refers to object `a`
        console.log(this);

        var setname = function (newname) {
            // `this` in here points to GLOBAL obj, unlike the previous `this`
            this.name = newname;
        }
        setname('Overriden again...or is it?');
        console.log(this);
    }
};

a.log();
console.log(a.name);


// bind new value to this (self = this)
var z = {
    name: "Name of 'z' object",
    log: function() {
        // self will ref to this
        var self = this;
        self.name = "Overidden name of 'z' object"
        console.log(self);

        var setname = function (newname) {
            self.name = newname;
        }
        setname('Overriden z name again...or is it? Now it is!');
        console.log(self);
    }
};

z.log();
console.log(z.name);


/**
 * ARRAY
 */

var arr = new Array();
// literal:
var arr = [
    1,
    false,
    {
        name: 'Jack',
        last: 'Bauer'
    },
    function(name) {
        var greet = 'Hi ';
        console.log(greet + name);
    },
    "hello"
];

console.log(arr);
arr[3](arr[2].name);


























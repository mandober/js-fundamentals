class Person {
    constructor(f, l) {
        this.f = f;
        this.l = l;
    }
    
    greet() {
        return 'Hi ' + this.f;
    }
}


var jo = new Person("Jack", "Bauer");
console.log(jo);
console.log(jo.f);
console.log(jo.greet());

class Another extends Person {
    constructor (f,l) {
        super(f,l);
    }
    
    greet() {
        return "Yo! " + this.f;
    }
    
}

var j = new Another("Terry", "Jones");
console.log(j);
console.log(j.l);
console.log(j.greet());
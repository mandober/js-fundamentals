name: js-fundamentals
repo: https://bitbucket.org/mandober/js-fundamentals


# JS Reference

## Types

**6 primitives:**
- boolean
- number
- string
- symbol
- undefined
- null
**1 native:**
- object

## Natives
- Object
- Function
- Array
- Date
- RegExp
- JSON
- etc.


# Features and concepts:

- JSON

- symbols
- proxy
- coercion
- scope
- hoisting
- `this` keyword
- `new` keyword
- destructuring
- iterators
- generators
- prototype chain
- recursion
- literal templates
- tagged templates
- array comprenhensions


## Inheretance
- parasitic inheritance
- prototypal inheritance
- mixins
- delegation







# Web APIs

https://developer.mozilla.org/en-US/docs/Web/API

  - DOM
  - WebWorkers
  - fetch


# Standard objects by category

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects

## Value properties

These global properties return a simple value; they have no properties or methods.
    Infinity
    NaN
    undefined
    null literal

## Function properties

These global functionsâ€”functions which are called globally rather than on an objectâ€”directly return their results to the caller.

    eval()
    uneval()
    isFinite()
    isNaN()
    parseFloat()
    parseInt()
    decodeURI()
    decodeURIComponent()
    encodeURI()
    encodeURIComponent()
    escape()
    unescape()

### Fundamental objects

These are the fundamental, basic objects upon which all other objects are based. This includes objects that represent general objects, functions, and errors.

    Object
    Function
    Boolean
    Symbol
    Error
    EvalError
    InternalError
    RangeError
    ReferenceError
    SyntaxError
    TypeError
    URIError

### Numbers and dates

These are the base objects representing numbers, dates, and mathematical calculations.

    Number
    Math
    Date

### Text processing

These objects represent strings and support manipulating them.

    String
    RegExp

### Indexed collections

These objects represent collections of data which are ordered by an index value. This includes (typed) arrays and array-like constructs.

    Array
    Int8Array
    Uint8Array
    Uint8ClampedArray
    Int16Array
    Uint16Array
    Int32Array
    Uint32Array
    Float32Array
    Float64Array

### Keyed collections

These objects represent collections which use keys; these contain elements which are iterable in the order of insertion.

    Map
    Set
    WeakMap
    WeakSet

### Vector collections

SIMD vector data types are objects where data is arranged into lanes.

    SIMD
    SIMD.Float32x4
    SIMD.Float64x2
    SIMD.Int8x16
    SIMD.Int16x8
    SIMD.Int32x4
    SIMD.Uint8x16
    SIMD.Uint16x8
    SIMD.Uint32x4
    SIMD.Bool8x16
    SIMD.Bool16x8
    SIMD.Bool32x4
    SIMD.Bool64x2

### Structured data

These objects represent and interact with structured data buffers and data coded using JavaScript Object Notation (JSON).

    ArrayBuffer
    SharedArrayBuffer
    Atomics
    DataView
    JSON

### Control abstraction objects

    Promise
    Generator
    GeneratorFunction
    AsyncFunction

### Reflection

    Reflect
    Proxy

### Internationalization

Additions to the ECMAScript core for language-sensitive functionalities.

    Intl
    Intl.Collator
    Intl.DateTimeFormat
    Intl.NumberFormat

### WebAssembly

    WebAssembly
    WebAssembly.Module
    WebAssembly.Instance
    WebAssembly.Memory
    WebAssembly.Table
    WebAssembly.CompileError
    WebAssembly.LinkError
    WebAssembly.RuntimeError

### Non-standard objects

    Iterator
    ParallelArray
    StopIteration

### Other

    arguments

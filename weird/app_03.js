/**
 * CLOSURES
 */

function greet(say) {

    return function(name) {
        console.log(say + name);
    }

}

greet('hi ')('tony');

var a = greet('hello ');
a('Jack');


// pt.2

// riddle
function buildFunctions() {
    var arr = [];
    for (var i = 0; i < 3; i++) {
        arr.push(
            function() {
                console.log(i);
            }
        )
    }
    return arr;
}
var fs = buildFunctions();
fs[0](); // 3
fs[1](); // 3
fs[2](); // 3


// to get what's "excpected"
function buildFunctions2() {
    var arr = [];
    for (var i = 0; i < 3; i++) {
        arr.push(
            (function(j) {
                return function() {
                    console.log(j);
                }
            }(i))
        )
    }
    return arr;
}
var fs2 = buildFunctions2();
fs2[0](); // 0
fs2[1](); // 1
fs2[2](); // 2





/**
 * call, apply, bind
 */

var person = {
    firstname: 'Jack',
    lastname: 'Bauer',
    getFullName: function() {
        var fullname = this.firstname + ' ' + this.lastname;
        return fullname;
    }
}

var logName = function(lang1, lang2) {
    console.log('Logged: ' + this.getFullName());
    console.log('Arguments: ' + lang1 + ' ' + lang2);
    console.log('-----------');
}

// bind (bound to person object)
var logPersonName = logName.bind(person);
logPersonName('en');

// call
logName.call(person, 'en', 'es');

// apply
logName.apply(person, ['en', 'es']);



(function(lang1, lang2) {
    console.log('Logged: ' + this.getFullName());
    console.log('Arguments: ' + lang1 + ' ' + lang2);
    console.log('-----------');
}).apply(person, ['es', 'en']);



// function borrowing
var person2 = {
    firstname: 'Jane',
    lastname: 'Doe'
}

console.log(person.getFullName.apply(person2));


// function currying
function multiply(a, b) {
    return a*b;
}

var multipleByTwo = multiply.bind(this, 2);
console.log(multipleByTwo(4));

var multipleByThree = multiply.bind(this, 3);
console.log(multipleByThree(4));



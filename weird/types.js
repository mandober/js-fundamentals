/**
 *
 */
;(function(global){
    
    'use strict';
    
    // new-up typo object
    var Typo = function(token) {
        return new Typo.init(token);
    };
    
    Typo.prototype = {
        
        // 'this' refers to the calling object at execution time
        isString: function() {
            return typeof(this.token) === 'string';   
        }
    };
    
    // the actual object is created here, allowing us 
    // to new up an object without calling new
    Typo.init = function(token) {
        var self = this;
        self.token = token || '';
    };
    
    // trick borrowed from jQuery so we don't have to use the 'new' keyword
    Typo.init.prototype = Typo.prototype;
    
    // attach our Typo to the global object, and provide shorthand '$$'
    global.Typo = global.$$ = Typo;
    
}(window));
/* Strict mode
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode

- makes it impossible to accidentally create global variables
- makes assignments which would otherwise silently fail throw an exception.
- makes attempts to delete undeletable properties throw (where before the attempt would simply have no effect)
- requires that function parameter names be unique
- forbids octal syntax
- forbids setting properties on primitive values
- prohibits `with`
- `this` defaults to undefined (instead of window) inside functions
- eval of strict mode code does not introduce new variables into the surrounding scope
- arguments.callee is no longer supported. In normal code arguments.callee refers to the enclosing function

*/
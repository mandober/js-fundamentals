"use strict";

/*
 * OBJECTS
 * objects_02_property_descriptors.js
 */


/*
PROPERTY DESCRIPTORS

  Object.defineProperty() method DEFINES A NEW PROPERTY directly on an object,
  or MODIFIES AN EXISTING PROPERTY on an object, and RETURNS the object.


SYNOPSIS:
  Object.defineProperty(obj, prop, descriptor)


PARAMETERS:

 obj
    The object on which to define the property.
    
 prop
    The name of the property to be defined or modified.
    
 descriptor
    The descriptor for the property being defined or modified:
    value, writable, configurable, enumerable, set, get


RETURN VALUE:
    The object that was passed to the function.


DESCRIPTION:
By default, values added using Object.defineProperty() are immutable.

Property descriptors present in objects come in two main flavors: 
1) data descriptor is a property that has a value, which may or may not be writable. 
2) accessor descriptor is a property described by a getter-setter pair of functions. 
   A descriptor must be one of these two flavors; it cannot be both.

Both data and accessor descriptors are objects. 
They share the following required keys:

configurable
    true if and only if the type of this property descriptor may be changed 
    and if the property may be deleted from the corresponding object.
    Defaults to false.
    
enumerable
    true if and only if this property shows up during enumeration of the
    properties on the corresponding object.
    Defaults to false.


DATA descriptor also has the following optional keys:

value
    The value associated with the property. Can be any valid JavaScript value (number, object, function, etc).
    Defaults to undefined.
    
writable
    true if and only if the value associated with the property may be changed with an assignment operator.
    Defaults to false.


ACCESSOR descriptor also has the following optional keys:

get
    A function which serves as a getter for the property, or undefined if there is no getter. 
    The function return will be used as the value of property.
    Defaults to undefined.
    
set
    A function which serves as a setter for the property, or undefined if there is no setter. 
    The function will receive as only argument the new value being assigned to the property.
    Defaults to undefined.


Bear in mind that these options are not necessarily the descriptor's own properties, and properties inherited from 
the prototype chain will be considered too. In order to ensure these defaults are preserved you might freeze the 
Object.prototype upfront, specify all options explicitly, or point to null as __proto__ property.

*/



var myObject1 = {
    a: 2
};

// Object.getOwnPropertyDescriptor(myObject1, "a");
//
// {
//   value: 2,
//   writable: true,
//   enumerable: true,
//   configurable: true
// }


var myObject2 = {};
// Object.defineProperty(obj, prop, descriptor)
Object.defineProperty(myObject2, "a", 
    {
        value: 2,
        writable: true,
        configurable: true,
        enumerable: true
    }
);
myObject2.a; // 2




// Writable

var myObject3 = {};
Object.defineProperty( myObject3, "a", {
    value: 2,
    writable: false, // not writable!
    configurable: true,
    enumerable: true
});

// this will trow an error in strict mode only (otherwise it will fail silently)
// myObject3.a = 3;
// myObject3.a; // still 2



/*
  If you want to prevent an object from having new properties added to it, but otherwise leave
  the rest of the object's properties alone, call Object.preventExtensions()
  Note that the properties of a non-extensible object, in general, may still be deleted. 
  Object.preventExtensions() only prevents addition of own properties, but properties can still 
  be added to the object prototype. However, calling Object.preventExtensions() on an object will 
  also prevent extensions on its __proto__ property.
  
Syntax
  Object.preventExtensions(obj)

Parameters

  obj
    The object which should be made non-extensible.

Return value
  The object being made non-extensible.
  
*/




// ITERATION

// for...of
var myArray1 = [ 1, 2, 3 ];

for (var v of myArray1) {
    console.log( v );
}
// Arrays have a built-in @@iterator so for..of works easily on them


//let's manually iterate the array, using the built-in @@iterator , to see how it works:
var myArray2 = [ 1, 2, 3 ];
var it = myArray2[Symbol.iterator]();
it.next(); // { value:1, done:false }
it.next(); // { value:2, done:false }
it.next(); // { value:3, done:false }
it.next(); // { done:true }


// It is possible to define your own default @@iterator for any object that you care to iterate over. 

var myObject = {
    a: 'a',
    b: 'b',
    c: 'c'
};

Object.defineProperty(myObject, Symbol.iterator, {
    enumerable: false,
    writable: false,
    configurable: true,
    value: function() {
        var o = this;
        var idx = 0;
        var ks = Object.keys(o);
        return {
            next: function(){
                return {
                    value: o[ks[idx++]],
                    done: (idx > ks.length)
                };
            }
        };
    }
});

// iterate `myObject` manually
var it = myObject[Symbol.iterator]();
it.next(); // { value:2, done:false }
it.next(); // { value:3, done:false }
it.next(); // { value:undefined, done:true }

// iterate `myObject` with `for..of`
for (var v of myObject) {
    console.log( v );
}


class Foo1 {
    
    constructor(a,b) {
        this.x = a;
        this.y = b;
    }
    
    gimmeXY() {
        return this.x * this.y;
    }
    
}


class Bar2 extends Foo1 {
    constructor(a,b,c) {
        super( a, b );
        this.z = c;
    }
    
    gimmeXYZ() {
        return super.gimmeXY() * this.z;
    }
}

var b1 = new Bar2( 5, 15, 25 );

console.log('b1.x : ', b1.x);
console.log('b1.y : ', b1.y);
console.log('b1.z : ', b1.z);
console.log('b1.gimmeXYZ() : ', b1.gimmeXYZ());




//new.target
class Foo {
    constructor() {
        console.log( "Foo: ", new.target.name );
    }
}

class Bar extends Foo {
    constructor() {
        super();
        console.log( "Bar: ", new.target.name );
    }
    
    baz() {
        console.log( "baz: ", new.target );
    }
}

var a = new Foo();  // Foo: Foo

var b = new Bar();  // Foo: Bar <-- respects `new` call-site
                    // Bar: Bar

b.baz();            // baz: undefined
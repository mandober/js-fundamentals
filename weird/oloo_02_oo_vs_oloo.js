"use strict";

/*
 * OLOO
 * oloo_02_oo_vs_oloo.js
 * 
 */


// classical ("prototypal") OO style:
function Foo(who) {
    this.me = who;
}

Foo.prototype.identify = function() {
    return "I am " + this.me;
};

function Bar(who) {
    Foo.call(this, who);
}

Bar.prototype = Object.create(Foo.prototype);

Bar.prototype.speak = function() {
    console.log("Hello, " + this.identify() + ".");
};

var b1 = new Bar("b1");
var b2 = new Bar("b2");

b1.speak();
b2.speak();




// the exact same functionality using OLOO style code:
var Foo2 = {
    init: function(who) {
        this.me = who;
    },
    identify: function() {
        return "I am " + this.me;
    }
};

var Bar2 = Object.create(Foo2);

Bar2.speak = function() {
    console.log("Hello, " + this.identify() + ".");
};

var b3 = Object.create(Bar2);
b3.init("b3");

var b4 = Object.create(Bar2);
b4.init("b4");

b3.speak();
b4.speak();
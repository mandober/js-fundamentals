/*
COMBINATORY LOGIC
http://raganwald.com/2015/02/13/functional-quantum-electrodynamics.html

the K, I, and V combinators, nicknamed the “Kestrel”, the “Idiot Bird”, and the “Vireo”

K = Kestrel = constant function maker
A constant function is a function that always returns the same thing, no matter what you give it.
For example, (x) => 42 is a constant function that always evaluates to 42. The kestrel, or K, is a
function that makes constant functions. You give it a value, and it returns a constant function 
that gives that value. Given two values, we can say that K always returns the first value: K(x)(y) => x

I = Idiot Bird = identity function
The identity function is a function that evaluates to whatever parameter you pass it. So I(42) => 42.

V = Vireo

*/
var I = (x) => (x);
var K = (x) => (y) => x;
var V = (x) => (y) => (a) => a(x)(y);
var B = (a, b) => (x) => a(b(x));




I('first'); /*?*/
K('first')('second'); /*?*/

// V('first')('second')(K) /*?*/
// V('first')('second')(K)(I) /*?*/

/*
K(I)
From what we just wrote, K(x)(y) => x, So K(I)(x) => I
What is K(I)(x)(y)? If K(I)(x) => I, then K(I)(x)(y) === I(y) which is y. Therefore, K(I)(x)(y) => y
Given two values, K(I) always returns the second value.
*/
K(I)("first")("second") /*?*/
K("first")("second") /*?*/

var first = K;
var second = K(I);/*?*/



// BACKWARDNESS

// If we represented a pair of values as an array, we’d write them like this:
var first = ([first, second]) => first;
var second = ([first, second]) => second;

var latin = ["primus", "secundus"];

first(latin) //=> "primus"
second(latin) //=> "secundus"

// but if we were using a POJO, we’d write them like this:
var first = ({ first, second }) => first;
var second = ({ first, second }) => second;

var latin = { first: "primus", second: "secundus" };

first(latin) //=> "primus"
second(latin) //=> "secundus"

// In both cases, the functions first and second know how the data is represented, whether
// it be an array or an object. You pass the data to these functions, and they extract it.

// But the first and second we built out of K and I don’t work that way. You call them and pass
// them the bits, and they choose what to return. So if we wanted to use them with a two-element
// array, we’d need to have a piece of code that calls some code.
K = (x) => (y) => x; /*?*/
var first = K; /*?*/
var second = K(I); /*?*/

var latin = (selector)  => selector("primus")("secundus");
//  latin = (first)     =>    first("primus")("secundus"); // primus
//  latin = (second)    =>   second("primus")("secundus"); // secundus

latin(first)  //=> "primus"
latin(second) //=> "secundus"

// Our latin data structure is no longer a dumb data structure, it’s a function.
// And instead of passing latin to first or second, we pass first or second to latin.
// It’s exactly backwards of the way we write functions that operate on data.


// VIREO

// Given that our latin data is represented as the function:
// (selector) => selector("primus")("secundus")
// our obvious next step is to make a function that makes data.

// For arrays, we’d write
// var = (first, second) => [first, second]

// For objects we’d write: 
// var = (first, second) => { first, second }

// In both cases, we take two parameters, and return the form of the data.

// For “data” we access with K and K(I), our “structure” is the function
// (selector) => selector("primus")("secundus")

// Let’s extract those into parameters:
// (first, second) => (selector) => selector(first)(second)

// For consistency with the way combinators are written as
// functions taking just one parameter, we’ll curry the function:
// (first) => (second) => (selector) => selector(first)(second)

// Let’s try it, we’ll use the word pair for the function that makes data 
// (When we need to refer to a specific pair, we’ll use the name aPair by default):
var first = K;
var second = K(I);
var pair = (first) => (second) => (selector) => selector(first)(second);

var latin = pair("primus")("secundus");

latin(first) //=> "primus"
latin(second) //=> "secundus"

// It works! Now what is this node function? 
// If we change the names to x, y, and z, we get: 
// (x) => (y) => (z) => z(x)(y)
// That’s the V combinator, the Vireo! So we can write:
var first = K;
var second = K(I);
var pair = V;

var latin = pair("primus")("secundus");

latin(first) //=> "primus"
latin(second)  //=> "secundus"

/* As an aside, the Vireo is a little like JavaScript’s `apply` function. It says, “take these two values and apply them to this function.” There are other, similar combinators that apply values to functions. One notable example is the “thrush” or T combinator: It takes one value and applies it to a function. It is known to most programmers as `tap`. */




// LISTS WITH FUNCTIONS AS DATA

// Here’s another look at linked lists using POJOs.
// We use the term rest instead of second, but it’s otherwise identical to what we have above:
var first = ({ first, rest }) => first,
    rest = ({ first, rest }) => rest,
    pair = (first, rest) => ({ first, rest }),
    EMPTY = ({});

var lst = pair(1, pair(2, pair(3, EMPTY)));

first(lst)/*?*/
first(rest(lst))/*?*/
first(rest(rest(lst))) /*?*/


// B COMBINATOR - COMPOSE
var B = (a, b) => (c) => a(b(c));



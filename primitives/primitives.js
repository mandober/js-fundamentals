// PRIMITIVES

/*
There are 6 primitive values in JS:
   * string
   * number
   * boolean
   * symbol
   * null
   * undefined

And 1 compound value:
   * object
which has a large number of subtypes (builtin objects or natives).

*/


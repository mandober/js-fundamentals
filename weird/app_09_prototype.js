String.prototype.isLarger = function () {
    return this.length > 2;
}
console.log("Peraper".isLarger());


Number.prototype.isPositive = function(){
    return this > 0;
}

// ERROR: expects float! (any number is internally a float in js)
//console.log(3.isPositive());

// solutions:
// use float
console.log(3.0.isPositive());
console.log(3..isPositive());
// use space
console.log(3 .isPositive());


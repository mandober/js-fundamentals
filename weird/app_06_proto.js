(function () {
    'use strict';
    
    var person = {
            firstname: 'Default ',
            lastname: 'Default',
            getFullName: function () {
                return this.firstname + this.lastname;  
            }
        },

        john = {
            firstname: 'Jack ',
            lastname: 'Bauer'
        };

    // don't do this EVER! for demo purposes only!!!
    john.__proto__ = person;
    console.log(john.getFullName());
    console.log(john.firstname);

    var jane = {
        firstname: 'Terry '   
    };

    jane.__proto__ = person;
    console.log(jane.getFullName());

    person.getFormalFullName = function() {
        return this.lastname + ', ' + this.firstname;   
    };

    console.log(john.getFormalFullName());
    console.log(jane.getFormalFullName());
     
    
}());
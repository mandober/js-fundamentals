// COMBINATORS
// https://leanpub.com/javascriptallongesix/read

// I
var I = (x) => (x);
// I or IDIOT BIRD is IDENTITY function:
// it evaluates to whatever parameter you pass it.
I(1); /*?*/


// K
var K = (x) => (y) => x;
// K or Kestrel is a constant function maker.
// A constant function is a function that always returns the same thing, no matter what you give it.
// For example, (x) => 42 is a constant function that always evaluates to 42.
// The kestrel, or K, is a function that makes constant functions.
// You give it a value, and it returns a constant function that gives that value.
// Given two values, we can say that K always returns the first value: K(x)(y) => x
K(1)(2); /*?*/


// K(I)
K(I)(1)(2) /*?*/

var K2 = K(I); /*?*/



var V = (x) => (y) => (a) => a(x)(y);
var B = (a, b) => (x) => a(b(x));
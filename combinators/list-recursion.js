/*
Lists and recursion
https://leanpub.com/javascriptallongesix/read

One of the simplest, and longest-standing in computer science, is to say that a list is:
1) Empty or
2) Consists of an element concatenated with a list

Converting these rules to array literals:
1) [] is a list.
2) this rule can be expressed using a spread operator:
   given an element `e` and a list `list`, `[e, ...list]` is a list
*/

[];                          //=> []
["baz", ...[]];              //=> ["baz"]
["bar", ...["baz"]];         //=> ["bar","baz"]
["foo", ...["bar", "baz"]];  //=> ["foo","bar","baz"]

// Thanks to the parallel between array literals + spreads with destructuring + rest operator,
// we can also use the same rules to decompose lists:
var [first, ...rest] = [];
first; //=> undefined
rest;  //=> []

var [first, ...rest] = ["foo"];
first; //=> "foo"
rest;  //=> []

var [first, ...rest] = ["foo", "bar"];
first; //=> "foo"
rest;  //=> ["bar"]

var [first, ...rest] = ["foo", "bar", "baz"];
first; /*?*/
rest;  /*?*/



// so the definition of an empty array could be stated as:
var isEmpty = ([first, ...rest]) => first === undefined;

isEmpty([]) /*?*/
isEmpty([0]) /*?*/
isEmpty([[]]) /*?*/



// and to calculate array's size recursively
var length = ([first, ...rest]) =>
    first === undefined
    ? 0
    : 1 + length(rest);

length([]) /*?*/
length(["foo"]) /*?*/
length(["foo", "bar", "baz"]) /*?*/



// this length function uses a functions to bind values to names, POJOs to structure nodes, 
// and the ternary function to detect the base case, the empty list.
var EMPTY = {};
var OneTwoThree = { first: 1, rest: { first: 2, rest: { first: 3, rest: EMPTY } } };
OneTwoThree.first /*?*/
OneTwoThree.rest.first /*?*/
OneTwoThree.rest.rest.first /*?*/

var length = (node, delayed = 0) =>
    node === EMPTY
    ? delayed
    : length(node.rest, delayed + 1);

length(OneTwoThree) /*?*/


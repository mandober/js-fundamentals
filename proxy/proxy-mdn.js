/* Builtins > Proxy
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy

handler   placeholder object which contains traps.
traps     methods that provide property access.
target    object which the proxy virtualizes; 

Syntax:
var p = new Proxy(target, handler);

Parameters:
target:
  target object (any object, including a native array, a function or even another proxy) to wrap with Proxy.
handler:
  object whose properties are functions which define the behavior of proxy when an operation is performed on it.

METHODS:
Proxy.revocable()  Creates a revocable Proxy object.

METHODS of the HANDLER object:
The handler object is a placeholder object which contains traps for Proxy.
All traps are optional. If a trap has not been defined, the default behavior
is to forward the operation to the target.

handler.getPrototypeOf()             trap for Object.getPrototypeOf
handler.setPrototypeOf()             trap for Object.setPrototypeOf
handler.isExtensible()               trap for Object.isExtensible
handler.preventExtensions()          trap for Object.preventExtensions
handler.getOwnPropertyDescriptor()   trap for Object.getOwnPropertyDescriptor
handler.defineProperty()             trap for Object.defineProperty
handler.has()                        trap for the in operator
handler.get()                        trap for getting property values
handler.set()                        trap for setting property values
handler.deleteProperty()             trap for the delete operator
handler.ownKeys()                    trap for Object.getOwnPropertyNames and Object.getOwnPropertySymbols
handler.apply()                      trap for a function call
handler.construct()                  trap for the new operator

*/

/**
 * ARGS
 */

function greet(first, last, lang) {
    console.log(first);
    console.log(last);
    console.log(lang);

    // arguments is array-like
    console.log(arguments);

    console.log('arg 1: ' +  arguments[1]);

}

greet("Jack", "Bauer", "eng");


// spread
function say(word, ...other) {
    console.log(arguments);
    console.log('arg 0: ' +  arguments[0]);
    console.log('arg 1: ' +  arguments[1]);
    console.log('arg rest: ' +  other);
}

say("Hi ", "there ", "boyo ", "now!");

// def. args
function say(word = "Hi") {
    console.log(word);
}

say();
say("Hello ");


// asi

function get() {
    // if the brace is on newline, error occurs due to ASI putting ; after return -> return;
    return {
        f: 1,
        l: 2
    };
}
console.log(get());


/**
 * IIFE
 */

var greet1 = function() {
    console.log("Hi from IIFE");
}();


var greet2 = function(name) {
    console.log("IIFE says hi to " + name);
}("Jack");


(function(){
    console.log("IIFE in parens");
})();

!function(){
    console.log("IIFE with a bang");
}();

+function(){
    console.log("IIFE with a plus");
}();



(function(global, name){
    global.aaa = "a test";
    console.log("IIFE in parens" + name);
})(window, "Jack");


(function(name){
    window.aaaa = "aa test";
    console.log("IIFE in parens" + name);
})("Jack");




